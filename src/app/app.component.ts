import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent
{
  private total = 200.0;
  private doadores = new Array<Doador>();

  private nomeDoador:string = "";
  private valorDoado:number = 0;

  getResta():number
  {
    let arrecadado = 0.0;
    for( let doador of this.doadores)
    {
      arrecadado += doador.valorDoado;
    }
    return this.total - arrecadado;
  }

  onAdicionarClicado():void
  {
    let doador:Doador = { nomeDoador: this.nomeDoador, valorDoado: this.valorDoado };

    this.doadores.push( doador );
    this.nomeDoador = "";
    this.valorDoado = 0;
  }

  isCamposValidos():boolean
  {
    return this.nomeDoador && this.nomeDoador.length > 3 && !isNaN(this.valorDoado);
  }
}

interface Doador
{
  nomeDoador:string,
  valorDoado:number
}
